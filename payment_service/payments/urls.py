from django.urls import path, register_converter

from .converts import FloatUrlParameterConverter
from .views import PaymentViewSet

register_converter(FloatUrlParameterConverter, 'float')

urlpatterns = [
    path(
        'pay/<int:user_id>/<float:amount>', PaymentViewSet.as_view(
            {
                'post': 'pay'
            }
        )
    ),
    path(
        'cancel/<int:user_id>/<int:order_id>', PaymentViewSet.as_view(
            {
                'post': 'cancel',
            }
        )
    ),
    path(
        'status/<int:order_id>', PaymentViewSet.as_view(
            {
                'get': 'status',
            }
        )
    ),
    path(
        'add_funds/<int:user_id>/<float:amount>', PaymentViewSet.as_view(
            {
                'post': 'add_funds',
            }
        )
    ),
    path(
        'create_user', PaymentViewSet.as_view(
            {
                'post': 'create_user',
            }
        )
    ),
    path(
        'find_user/<int:user_id>', PaymentViewSet.as_view(
            {
                'get': 'find_user',
            }
        )
    ),
]
