from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Payment, User
from .producer import publish_order_queue
from .serializers import UserSerializer


class PaymentViewSet(viewsets.ViewSet):
    """
    POST - subtracts the amount of the order from the user’s credit (returns failure if credit is not enough)
    """

    def pay(self, request, user_id, order_id, amount):
        order = get_object_or_404(Payment, pk=int(order_id))
        payment = Payment.objects.filter(id=int(order_id), user_id=int(user_id))
        user = get_object_or_404(pk=int(user_id))

        if payment.amount < user.credit:
            return Response(status=401)

        return Response(status=404)

    """
    POST - cancels payment made by a specific user for a specific order.
    """

    def cancel(self, request, user_id, order_id):
        payment = Payment.objects.filter(id=int(order_id), user_id=int(user_id))
        payment.delete()
        return Response(status=200)

    """
    GET - returns the status of the payment (paid or not)
    Output JSON fields:
        “paid” - (true/false)
    """

    def status(self, request, order_id):
        try:
            payment = Payment.objects.get(id=order_id)
            return JsonResponse({'paid': payment.is_paid}, status=200)
        except Payment.DoesNotExist:
            return JsonResponse({'paid': False}, status=200)

    """
    POST - adds the amount of the order from the user’s credit (returns failure if credit is not enough)
    Output JSON fields:
        “done” - (true/false)
    """

    def add_funds(self, request, user_id, amount):
        user = get_object_or_404(User, pk=user_id)
        user.credit += amount
        user.save()

        return JsonResponse({'done': True}, status=200)

    """
    POST - adds the amount of the order from the user’s credit (returns failure if credit is not enough)
    Output JSON fields:
            “done” - (true/false)
    """

    def create_user(self, request):
        user = User(credit=0)
        user.save()

        serializer = UserSerializer(user)

        publish_order_queue('user_created', serializer.data)
        return JsonResponse({'user_id': user.id}, status=200)

    """
    GET - returns the user information
    Output JSON fields:
        “user_id” - the user’s id
        “credit” - the user’s credit
    """

    def find_user(self, request, user_id):
        user = get_object_or_404(User, pk=int(user_id))
        serializer = UserSerializer(user)

        return Response(serializer.data, status=200)
