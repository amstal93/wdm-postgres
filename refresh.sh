#!/bin/bash

docker-compose restart order_db
docker-compose exec order_service python manage.py reset_db --noinput
docker-compose exec order_service python manage.py makemigrations
docker-compose exec order_service python manage.py migrate

docker-compose restart payment_db
docker-compose exec payment_service python manage.py reset_db --noinput
docker-compose exec payment_service python manage.py makemigrations
docker-compose exec payment_service python manage.py migrate

docker-compose restart stock_db
docker-compose exec stock_service python manage.py reset_db --noinput
docker-compose exec stock_service python manage.py makemigrations
docker-compose exec stock_service python manage.py migrate
