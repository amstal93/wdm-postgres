#!/bin/sh

echo "Waiting for order service..."
while ! nc -z $ORDER_SERVICE_HOST $ORDER_SERVICE_PORT; do
  sleep 5
done
echo "Order service started"

exec "$@"
