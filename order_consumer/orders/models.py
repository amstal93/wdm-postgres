from django.db import models


class User(models.Model):
    id = models.BigIntegerField(primary_key=True)


class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    cost = models.FloatField(default=0.0)


class Order(models.Model):
    id = models.BigAutoField(primary_key=True)
    is_paid = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField(Item, through='OrderItemMembership')


class OrderItemMembership(models.Model):
    id = models.BigAutoField(primary_key=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    count = models.IntegerField(default=1)
