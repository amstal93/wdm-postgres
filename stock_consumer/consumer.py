import json
import os

import django
import pika

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

from stock.models import Item


def on_rpc_message(ch, method, props, body):
    data = json.loads(body)

    items = []
    is_reduced = False
    response = {'content_type': 'stock_reduce_failure'}

    for idx, item in enumerate(data['items']):
        it = Item.objects.get(id=item)

        if it.stock > 0 and it.stock >= data['item_counts'][idx]:
            it.stock -= data['item_counts'][idx]
            items.append(it)

            response = {'content_type': 'stock_reduced'}
            is_reduced = True

        else:
            is_reduced = False
            break

    if is_reduced:
        Item.objects.bulk_update(items, ['stock'])

    ch.basic_publish(
        exchange='',
        routing_key=props.reply_to,
        properties=pika.BasicProperties(
            priority=9,
            correlation_id= \
                props.correlation_id
        ),
        body=json.dumps(response)
    )


credentials = pika.PlainCredentials(
    username=os.environ.get('RABBITMQ_USERNAME'),
    password=os.environ.get('RABBITMQ_PASSWORD')
)

params = pika.ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST'),
    port=os.environ.get('RABBITMQ_PORT'),
    credentials=credentials,
    heartbeat=600,
    blocked_connection_timeout=300
)

connection = pika.BlockingConnection(params)

channel = connection.channel()
channel.queue_declare(queue='stock_rpc_queue')
channel.basic_consume(queue='stock_rpc_queue', on_message_callback=on_rpc_message, auto_ack=True)

try:
    print('Started consuming')
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()

connection.close()
