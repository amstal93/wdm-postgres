# WDM CockroachDB

This project demonstrates the use of microservices with PostgreSQL. There are three services, each of which contains a Python Django web server. All three services have their own PostgreSQL database.

The microservices communicate via asynchronous message publishing and subscribing, where messages are sent through RabbitMQ. 
 
Additionally, a SAGA is implemented in the checkout system with RPC to improve consistency.

## Requirements

You must have a machine with docker running and docker-compose to run this project.

## Installation:

Clone the project, and enter the project directory.

To install run: `./install.sh`

## Usage

To run the development version, run `./start.sh`

To run the production version, run `./start-prod.sh`

If you need to refresh the database (clear all rows in the tables), run `./refresh.sh`
