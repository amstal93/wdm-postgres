#!/bin/sh

if [ "$DATABASE" = "postgres" ]; then
  echo "Waiting for PostgreSQL..."

  while ! nc -z $SQL_HOST $SQL_PORT; do
    sleep 5
  done

  echo "PostgreSQL started"
fi

echo "Waiting for RabbitMQ..."
while ! nc -z $RABBITMQ_HOST $RABBITMQ_PORT; do
  sleep 5
done
echo "RabbitMQ started"

python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate

exec "$@"
