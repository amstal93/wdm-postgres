from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Item
from .producer import publish_order_queue
from .serializers import ItemSerializer


class StockViewSet(viewsets.ViewSet):
    """
    GET - returns an item’s availability and price.
    Output JSON fields:
        “stock” - the item’s stock
        “price” - the item’s price
    """

    def find(self, request, item_id):
        item = Item.objects.get(id=int(item_id))
        serializer = ItemSerializer(item)
        return Response(serializer.data)

    """
    POST - subtracts an item from stock by the amount specified.
    """

    def subtract(self, request, item_id, number):
        item = get_object_or_404(Item, pk=int(item_id))
        item.stock -= int(number)
        item.stock = max(0, item.stock)
        item.save()

        return Response(status=200)

    """
    POST - adds the given number of stock items to the item count in the stock
    """

    def add(self, request, item_id, number):
        item = get_object_or_404(Item, pk=int(item_id))
        item.stock += number
        item.save()

        return Response(status=200)

    """
    POST - adds an item and its price, and returns its ID.
    Output JSON fields:
    “item_id” - the item’s id
    """

    def create(self, request, price):
        item = Item.objects.create(cost=float(price))
        serializer = ItemSerializer(instance=item)
        item.save()

        publish_order_queue('items_created', serializer.data)
        return JsonResponse({'item_id': item.id}, status=200)
