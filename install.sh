#!/bin/bash

# Remove relevant containers, volumes and images
if [ ! "docker container ls -aq --filter \"name=wdm-postgres\"" ]; then
  docker container stop $(docker container ls -aq --filter "name=wdm-postgres")
  docker container rm -f --volumes $(docker container ls -aq --filter "name=wdm-postgres")
  docker image prune -f
fi

[ ! "docker network ls --filter \"name=wdm-postgres_default\"" ] && docker network rm wdm-postgres_default

# Build docker compose
docker-compose build --no-cache
docker-compose -f docker-compose.prod.yml build --no-cache
